package com.company.chess;


import com.company.chess.gui.board.BoardView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Chess");

        BoardView boardView = new BoardView();
        StackPane mainPane = new StackPane();
        mainPane.getChildren().addAll(boardView.getImageView(), boardView.getBoardGrid());

        primaryStage.setScene(new Scene(mainPane));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String args[]){
        launch(args);
    }
}