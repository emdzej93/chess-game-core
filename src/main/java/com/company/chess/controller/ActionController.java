package com.company.chess.controller;

import com.company.chess.core.board.Board;
import com.company.chess.core.board.Tile;
import com.company.chess.core.pieces.Pawn;
import com.company.chess.core.validator.MoveValidator;

public class ActionController {
    private Board board = Board.getInstance();
    private MoveValidator moveValidator = new MoveValidator();

    public void executeAction(int row, int column){
        //System.out.println("Row: " +  board.getTile(row, column).getRow() + " Col: " +  board.getTile(row, column).getColumn());
        if(board.getSelectedTile() == null){
            selectTile(row, column);
        } else {
            switch(moveValidator.getMoveType(row, column)) {
                case ILLEGAL_MOVE: {
                    System.out.println("illegal move");
                    break;
                }

                case NEUTRAL_MOVE: {
                    System.out.println("neutral move");
                    movePiece(row, column);
                    break;
                }

                case ATTACK_MOVE: {
                    System.out.println("attack move");
                    capturePiece(row, column);
                    break;
                }
            }
            board.unselectAllPieces();
        }
    }

    private void movePiece(int newRow, int newColumn){
        int oldRow = board.getSelectedTile().getRow();
        int oldColumn = board.getSelectedTile().getColumn();
        if(board.getSelectedTile().getPiece() instanceof Pawn){
            ((Pawn) board.getSelectedTile().getPiece()).reinitLegalMoves();
        }
        board.getTile(newRow, newColumn).setPiece(board.getSelectedTile().getPiece());
        board.getTile(oldRow, oldColumn).setPiece(null);
    }

    private void capturePiece(int newRow, int newColumn){
        //TODO - manage to remove piece from player score
        int oldRow = board.getSelectedTile().getRow();
        int oldColumn = board.getSelectedTile().getColumn();
        if(board.getSelectedTile().getPiece() instanceof Pawn){
            ((Pawn) board.getSelectedTile().getPiece()).reinitLegalMoves();
        }
        board.getTile(newRow, newColumn).setPiece(board.getSelectedTile().getPiece());
        board.getTile(oldRow, oldColumn).setPiece(null);
    }

    private void selectTile(int row, int column) {
        board.selectPiece(row, column);
    }

    public Tile[][] getTiles() {
        return board.getTiles();
    }
}
