package com.company.chess.gui.board;

import com.company.chess.controller.ActionController;
import com.company.chess.gui.pieces.Piece;
import com.company.chess.gui.pieces.PieceFactory;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

public class BoardView {
    private ImageView imageView;
    private GridPane boardGrid;
    private TileView[][] tiles;
    private ActionController actionController;

    private final double BOARD_WIDTH = 480;
    private final double BOARD_HEIGHT = 480;
    private final int ROWS = 8;
    private final int COLUMNS = 8;

    public BoardView() {
        actionController = new ActionController();
        imageView = new ImageView(new Image("board.png"));
        tiles = new TileView[ROWS][COLUMNS];
        boardGrid = initBoardView();
        renderPieces();
    }

    private GridPane initBoardView() {
        GridPane boardGrid = new GridPane();
        boardGrid.setPrefSize(BOARD_WIDTH, BOARD_HEIGHT);

        double tileWidth = 480 / ROWS;
        double tileHeight = 480 / COLUMNS;

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                tiles[i][j] = new TileView(i, j);
                tiles[i][j].setPrefSize(tileWidth, tileHeight);
                boardGrid.add(tiles[i][j], j, i);
            }
        }
        return boardGrid;
    }

    public void renderPieces() {
        for(int i = 0 ; i < ROWS; i++){
            for(int j = 0; j < COLUMNS; j++){
                tiles[i][j].getChildren().clear();
                if(actionController.getTiles()[i][j].isOccupied()) {
                    Piece piece = PieceFactory.getPiece(actionController.getTiles()[i][j].getPiece());
                    tiles[i][j].getChildren().add(piece.getShape());
                    tiles[i][j].setAlignment(piece.getShape(), Pos.BOTTOM_RIGHT);
                    piece.setSelected(actionController.getTiles()[i][j].getPiece().isSelected());
                }
            }
        }
    }

    class TileView extends StackPane {
        private final int row;
        private final int column;

        public TileView(int row, int column) {
            this.row = row;
            this.column = column;
            setOnMouseClicked(e -> {
                actionController.executeAction(this.row, this.column);
                renderPieces();
            });
        }

        @Override
        public boolean equals(Object object) {
            TileView tile = (TileView)object;
            if(this.row == tile.row && this.column == tile.column) {
                return true;
            }
            return false;
        }
    }

    public GridPane getBoardGrid(){
        return boardGrid;
    }

    public ImageView getImageView() {
        return imageView;
    }
}
