package com.company.chess.gui.pieces;

public class Bishop extends Piece {
    Bishop(String imagePath){
        super(imagePath);
    }
    public static class BlackBishop extends Bishop {
        public BlackBishop(){
            super("pieces/black_bishop.png");
        }
    }

    public static class WhiteBishop extends Bishop {
        public WhiteBishop(){
            super("pieces/white_bishop.png");
        }
    }
}
