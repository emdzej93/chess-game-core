package com.company.chess.gui.pieces;

public class Knight extends Piece {
    Knight(String imagePath){
        super(imagePath);
    }
    public static class BlackKnight extends Knight {
        public BlackKnight(){
            super("pieces/black_knight.png");
        }
    }

    public static class WhiteKnight extends Knight {
        public WhiteKnight(){
            super("pieces/white_knight.png");
        }
    }
}
