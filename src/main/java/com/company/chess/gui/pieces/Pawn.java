package com.company.chess.gui.pieces;

public class Pawn extends Piece {

    Pawn(String imagePath){
        super(imagePath);
    }
    public static class BlackPawn extends Pawn {
        public BlackPawn(){
            super("pieces/black_pawn.png");
        }
    }

    public static class WhitePawn extends Pawn {
        public WhitePawn(){
            super("pieces/white_pawn.png");
        }
    }
}
