package com.company.chess.gui.pieces;

public class King extends Piece {
    King(String imagePath){
        super(imagePath);
    }
    public static class BlackKing extends King {
        public BlackKing(){
            super("pieces/black_king.png");
        }
    }

    public static class WhiteKing extends King {
        public WhiteKing(){
            super("pieces/white_king.png");
        }
    }
}
