package com.company.chess.gui.pieces;

import com.company.chess.core.pieces.PieceColor;
import com.company.chess.core.pieces.Bishop;
import com.company.chess.core.pieces.King;
import com.company.chess.core.pieces.Knight;
import com.company.chess.core.pieces.Pawn;
import com.company.chess.core.pieces.Queen;
import com.company.chess.core.pieces.Rook;

public class PieceFactory {

    public static Piece getPiece(com.company.chess.core.pieces.Piece piece){
        if(piece instanceof Pawn) {
            if(piece.getColor() == PieceColor.WHITE) {
                return new com.company.chess.gui.pieces.Pawn.WhitePawn();
            } else {
                return new com.company.chess.gui.pieces.Pawn.BlackPawn();
            }
        }

        if(piece instanceof Rook) {
            if(piece.getColor() == PieceColor.WHITE) {
                return new com.company.chess.gui.pieces.Rook.WhiteRook();
            } else {
                return new com.company.chess.gui.pieces.Rook.BlackRook();
            }
        }

        if(piece instanceof Knight) {
            if(piece.getColor() == PieceColor.WHITE) {
                return new com.company.chess.gui.pieces.Knight.WhiteKnight();
            } else {
                return new com.company.chess.gui.pieces.Knight.BlackKnight();
            }
        }

        if(piece instanceof Bishop) {
            if(piece.getColor() == PieceColor.WHITE) {
                return new com.company.chess.gui.pieces.Bishop.WhiteBishop();
            } else {
                return new com.company.chess.gui.pieces.Bishop.BlackBishop();
            }
        }

        if(piece instanceof Queen) {
            if(piece.getColor() == PieceColor.WHITE) {
                return new com.company.chess.gui.pieces.Queen.WhiteQueen();
            } else {
                return new com.company.chess.gui.pieces.Queen.BlackQueen();
            }
        }

        if(piece instanceof King) {
            if(piece.getColor() == PieceColor.WHITE) {
                return new com.company.chess.gui.pieces.King.WhiteKing();
            } else {
                return new com.company.chess.gui.pieces.King.BlackKing();
            }
        }

        return null;
    }
}
