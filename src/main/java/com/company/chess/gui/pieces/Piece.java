package com.company.chess.gui.pieces;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

public abstract class Piece {
    private Circle shape;

    public Piece(String imagePath){
        shape = new Circle(25);
        shape.setFill(new ImagePattern(new Image(imagePath)));
        shape.setStroke(Color.BLACK);
    }

    public void setSelected(boolean value){
        if(value){
            shape.setStrokeWidth(3);
        } else {
            shape.setStrokeWidth(1);
        }
    }

    public Circle getShape() {
        return shape;
    }

}
