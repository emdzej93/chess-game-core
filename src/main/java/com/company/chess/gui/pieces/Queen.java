package com.company.chess.gui.pieces;

public class Queen extends Piece {
    Queen(String imagePath){
        super(imagePath);
    }
    public static class BlackQueen extends Queen {
        public BlackQueen(){
            super("pieces/black_queen.png");
        }
    }

    public static class WhiteQueen extends Queen {
        public WhiteQueen(){
            super("pieces/white_queen.png");
        }
    }
}
