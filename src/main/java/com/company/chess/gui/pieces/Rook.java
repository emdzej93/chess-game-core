package com.company.chess.gui.pieces;

public class Rook extends Piece{
    Rook(String imagePath){
        super(imagePath);
    }
    public static class BlackRook extends Rook {
        public BlackRook(){
            super("pieces/black_rook.png");
        }
    }

    public static class WhiteRook extends Rook {
        public WhiteRook(){
            super("pieces/white_rook.png");
        }
    }
}
