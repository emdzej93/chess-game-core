package com.company.chess.core.pieces;

public class King extends Piece {
    public King(PieceColor color) {
        super(color);
    }

    @Override
    void initLegalMoves() {
        legalMoves.add(new int[] {1, 0});
        legalMoves.add(new int[] {-1, 0});
        legalMoves.add(new int[] {0, 1});
        legalMoves.add(new int[] {0, -1});
        legalMoves.add(new int[] {1, 1});
        legalMoves.add(new int[] {1, -1});
        legalMoves.add(new int[] {-1, 1});
        legalMoves.add(new int[] {-1, -1});
    }

    @Override
    void initAttackMoves() {
        attackMoves.addAll(legalMoves);
    }
}
