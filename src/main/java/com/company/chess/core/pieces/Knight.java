package com.company.chess.core.pieces;

public class Knight extends Piece {
    public Knight(PieceColor color){
        super(color);
    }

    @Override
    void initLegalMoves() {
        legalMoves.add(new int[] {2, 1});
        legalMoves.add(new int[] {2, -1});
        legalMoves.add(new int[] {-2, 1});
        legalMoves.add(new int[] {-2, -1});
        legalMoves.add(new int[] {1, 2});
        legalMoves.add(new int[] {1, -2});
        legalMoves.add(new int[] {-1, 2});
        legalMoves.add(new int[] {-1, -2});
    }

    @Override
    void initAttackMoves() {
        attackMoves.addAll(legalMoves);
    }
}
