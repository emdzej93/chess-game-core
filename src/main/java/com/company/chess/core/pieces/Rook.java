package com.company.chess.core.pieces;

public class Rook extends Piece {
    public Rook(PieceColor color) {
        super(color);
    }

    @Override
    void initLegalMoves() {
        for(int i = 0; i < 8; i++){
            legalMoves.add(new int[] {-i, 0});
            legalMoves.add(new int[] {i, 0});
            legalMoves.add(new int[] {0, -i});
            legalMoves.add(new int[] {0, i});
        }
    }

    @Override
    void initAttackMoves() {
        attackMoves.addAll(legalMoves);
    }
}
