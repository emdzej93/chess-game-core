package com.company.chess.core.pieces;

public class Pawn extends Piece {
    public Pawn(PieceColor color){
        super(color);
    }

    public void reinitLegalMoves(){
        legalMoves.clear();
        firstMove = false;
        initLegalMoves();
    }

    @Override
    void initLegalMoves() {
        if(color == PieceColor.WHITE) {
            legalMoves.add(new int[] {-1, 0});
            if(firstMove){
                legalMoves.add(new int[] {-2, 0});
            }
        } else {
            legalMoves.add(new int[] {1, 0});
            if(firstMove){
                legalMoves.add(new int[] {2, 0});
            }
        }
    }

    @Override
    void initAttackMoves() {
        if(color == PieceColor.WHITE) {
            attackMoves.add(new int[] {-1, -1});
            attackMoves.add(new int[] {-1, 1});
        } else {
            attackMoves.add(new int[] {1, -1});
            attackMoves.add(new int[] {1, 1});
        }
    }
}
