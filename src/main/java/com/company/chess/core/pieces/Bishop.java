package com.company.chess.core.pieces;

public class Bishop extends Piece {
    public Bishop(PieceColor color){
        super(color);
    }

    @Override
    void initLegalMoves() {
        for(int i = 0 ; i < 8; i++){
            legalMoves.add(new int[] {-i, -i});
            legalMoves.add(new int[] {-i, i});
            legalMoves.add(new int[] {i, -i});
            legalMoves.add(new int[] {i, i});
        }
    }

    @Override
    void initAttackMoves() {
        attackMoves.addAll(legalMoves);
    }
}
