package com.company.chess.core.pieces;

public enum PieceColor {
    BLACK, WHITE
}
