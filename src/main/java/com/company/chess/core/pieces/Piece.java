package com.company.chess.core.pieces;


import java.util.HashSet;
import java.util.Set;

public abstract  class Piece {
    boolean firstMove = true;
    final PieceColor color;
    private boolean isSelected;
    private boolean isCaptured;
    //First element of array represents row, second element of array represents column
    Set<int[]> legalMoves = new HashSet<>();
    Set<int[]> attackMoves = new HashSet<>();

    abstract void initLegalMoves();
    abstract void initAttackMoves();

    Piece(PieceColor color){
        this.color = color;
        this.isSelected = false;
        initLegalMoves();
        initAttackMoves();
    }

    public Piece(Piece piece) {
       this.color = piece.getColor();
    }

    public void select(boolean arg){
        this.isSelected = arg;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public Set<int[]> getLegalMoves() {
        return legalMoves;
    }

    public Set<int[]> getAttackMoves() {
        return attackMoves;
    }

    public boolean isCaptured() {
        return isCaptured;
    }

    public PieceColor getColor() {
        return color;
    }
}
