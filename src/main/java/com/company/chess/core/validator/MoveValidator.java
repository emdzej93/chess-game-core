package com.company.chess.core.validator;

import com.company.chess.core.board.Board;
import com.company.chess.core.pieces.Knight;

import java.util.*;

public class MoveValidator {
    private Board board = Board.getInstance();

    public MoveType getMoveType(int newRow, int newColumn){
        int oldRow = board.getSelectedTile().getRow();
        int oldColumn = board.getSelectedTile().getColumn();

        if(isLegalMove(oldRow, oldColumn, newRow, newColumn) && !isTileOccupied(newRow, newColumn)){
            if(checkCollision(oldRow, oldColumn, newRow, newColumn)) {
                return MoveType.NEUTRAL_MOVE;
            }
        }

        if(isTileOccupied(newRow, newColumn)) {
            if(isAttackMove(oldRow, oldColumn, newRow, newColumn)) {
                if(checkCollision(oldRow, oldColumn, newRow, newColumn)) {
                    return MoveType.ATTACK_MOVE;
                }
            }
        }

        return MoveType.ILLEGAL_MOVE;
    }

    private boolean isLegalMove(int oldRow, int oldColumn, int newRow, int newColumn){
        int rowDifference = newRow - oldRow;
        int columnDifference = newColumn - oldColumn;

        for(int[] legalMoves :  board.getSelectedTile().getPiece().getLegalMoves()) {
            if(Arrays.equals(legalMoves, new int[] {rowDifference, columnDifference})) {
                return true;
            }
        }
        return false;
    }

    private boolean isAttackMove(int oldRow, int oldColumn, int newRow, int newColumn){
        int rowDifference = newRow - oldRow;
        int columnDifference = newColumn - oldColumn;

        if(board.getTile(oldRow, oldColumn).getPiece().getColor() ==
                board.getTile(newRow, newColumn).getPiece().getColor()) {
            return false;
        }

        for(int[] legalMoves :  board.getSelectedTile().getPiece().getAttackMoves()) {
            if(Arrays.equals(legalMoves, new int[] {rowDifference, columnDifference})) {
                return true;
            }
        }
        return false;
    }

    private boolean isTileOccupied(int newRow, int newColumn){
        return board.getTile(newRow, newColumn).isOccupied();
    }

    private boolean checkCollision(int oldRow, int oldColumn, int newRow, int newColumn) {
        if(board.getSelectedTile().getPiece() instanceof Knight) {
            return true;
        }
        //Moving up
        if(oldColumn == newColumn && oldRow > newRow) {
            for(int i = oldRow - 1; i > newRow; i--){
                if(board.getTile(i, oldColumn).isOccupied()) {
                    return false;
                }
            }
        }

        //Moving down
        if(oldColumn == newColumn && oldRow < newRow) {
            for(int i = oldRow + 1; i < newRow; i++){
                if(board.getTile(i, oldColumn).isOccupied()) {
                    return false;
                }
            }
        }

        //Moving left
        if(oldRow == newRow && oldColumn > newColumn) {
            for(int i = oldColumn - 1; i > newColumn; i--){
                if(board.getTile(oldRow, i).isOccupied()) {
                    return false;
                }
            }
        }

        //Moving right
        if(oldRow == newRow && oldColumn < newColumn) {
            for(int i = oldColumn + 1; i < newColumn; i++){
                if(board.getTile(oldRow, i).isOccupied()) {
                    return false;
                }
            }
        }

        //Moving up-left
        if(oldRow > newRow && oldColumn > newColumn) {
            List<int[]> tilesToCheck = new ArrayList<>();
            int tilesToCheckIndex = 0;

            for(int i = oldRow - 1; i > newRow; i--) {
                tilesToCheck.add(new int[] {i, 0});
            }

            for(int j = oldColumn - 1; j > newColumn; j--){
                tilesToCheck.get(tilesToCheckIndex)[1] = j;
                tilesToCheckIndex++;
            }

            for(int[] tileToCheck : tilesToCheck){
                if(board.getTile(tileToCheck[0], tileToCheck[1]).isOccupied()) {
                    return false;
                }
            }
        }

        //Moving up-right
        if(oldRow > newRow && oldColumn < newColumn) {
            List<int[]> tilesToCheck = new ArrayList<>();
            int tilesToCheckIndex = 0;

            for(int i = oldRow - 1; i > newRow; i--) {
               tilesToCheck.add(new int[] {i, 0});
            }

            for(int j = oldColumn + 1; j < newColumn; j++){
                tilesToCheck.get(tilesToCheckIndex)[1] = j;
                tilesToCheckIndex++;
            }

            for(int[] tileToCheck : tilesToCheck){
                if(board.getTile(tileToCheck[0], tileToCheck[1]).isOccupied()) {
                    return false;
                }
            }
        }

        //Moving down-left
        if(oldRow < newRow && oldColumn > newColumn) {
            List<int[]> tilesToCheck = new ArrayList<>();
            int tilesToCheckIndex = 0;

            for(int i = oldRow + 1; i < newRow; i++) {
                tilesToCheck.add(new int[] {i, 0});
            }

            for(int j = oldColumn - 1; j > newColumn; j--){
                tilesToCheck.get(tilesToCheckIndex)[1] = j;
                tilesToCheckIndex++;
            }

            for(int[] tileToCheck : tilesToCheck){
                if(board.getTile(tileToCheck[0], tileToCheck[1]).isOccupied()) {
                    return false;
                }
            }
        }

        //Moving down-right
        if(oldRow < newRow && oldColumn < newColumn) {
            List<int[]> tilesToCheck = new ArrayList<>();
            int tilesToCheckIndex = 0;

            for(int i = oldRow + 1; i < newRow; i++) {
                tilesToCheck.add(new int[] {i, 0});
            }

            for(int j = oldColumn + 1; j < newColumn; j++){
                tilesToCheck.get(tilesToCheckIndex)[1] = j;
                tilesToCheckIndex++;
            }

            for(int[] tileToCheck : tilesToCheck){
                if(board.getTile(tileToCheck[0], tileToCheck[1]).isOccupied()) {
                    return false;
                }
            }
        }

        return true;
    }
}
