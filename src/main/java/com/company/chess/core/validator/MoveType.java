package com.company.chess.core.validator;

public enum MoveType {
    NEUTRAL_MOVE, ATTACK_MOVE, ILLEGAL_MOVE, CHECK_MOVE, CHECKMATE_MOVE
}
