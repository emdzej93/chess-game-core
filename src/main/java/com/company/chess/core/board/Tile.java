package com.company.chess.core.board;

import com.company.chess.core.pieces.Piece;

public final class Tile {
    private final int row;
    private final int column;
    private Piece piece;

    public Tile(int row, int column){
        this.row = row;
        this.column = column;
    }

    public boolean isOccupied(){
        if(piece == null){
            return false;
        }
        return true;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

}
