package com.company.chess.core.board;

import com.company.chess.core.pieces.*;

public class Board {
    private static final int ROWS = 8;
    private static final int COLUMNS = 8;
    private static Board instance = null;
    private Tile[][] tiles;

    public static Board getInstance() {
        if(instance == null) {
            instance = new Board();
        }
        return instance;
    }

    public void prepareBoard(){
        createBoard();
        createPieces();
    }

    public Tile getTile(int row, int column) {
        return tiles[row][column];
    }

    public Tile[][] getTiles() {
        return tiles;
    }

    public void selectPiece(int row, int column) {
        unselectAllPieces();
        if(getTile(row, column).getPiece() != null){
            getTile(row, column).getPiece().select(true);
        }
    }

//    public Piece getSelectedPiece() {
//        for(int i = 0 ; i < ROWS; i++ ){
//            for(int j = 0 ; j < COLUMNS; j++){
//                if(tiles[i][j].getPiece() != null) {
//                    if(tiles[i][j].getPiece().isSelected()) {
//                        return tiles[i][j].getPiece();
//                    }
//                }
//            }
//        }
//        return null;
//    }

    public Tile getSelectedTile() {
        for(int i = 0; i < ROWS; i++){
            for(int j = 0; j < COLUMNS; j++){
                if(tiles[i][j].getPiece() != null) {
                    if(tiles[i][j].getPiece().isSelected()) {
                        return tiles[i][j];
                    }
                }
            }
        }
        return null;
    }

    public void unselectAllPieces(){
        for(int i = 0 ; i < ROWS; i++ ){
            for(int j = 0 ; j < COLUMNS; j++){
                if(tiles[i][j].getPiece() != null){
                    tiles[i][j].getPiece().select(false);
                }
            }
        }
    }

    private Board(){
        tiles = new Tile[ROWS][COLUMNS];
        prepareBoard();
    }

    private void createBoard(){
        for(int i = 0 ; i < ROWS; i++){
            for(int j = 0 ; j < COLUMNS; j++) {
                tiles[i][j] = new Tile(i, j);
            }
        }
    }

    private void createPieces(){
        //Pawns
        for(int i = 0; i < COLUMNS; i++){
            tiles[1][i].setPiece(new Pawn(PieceColor.BLACK));
            tiles[6][i].setPiece(new Pawn(PieceColor.WHITE));
        }

        //Rooks
        tiles[0][0].setPiece(new Rook(PieceColor.BLACK));
        tiles[0][7].setPiece(new Rook(PieceColor.BLACK));
        tiles[7][0].setPiece(new Rook(PieceColor.WHITE));
        tiles[7][7].setPiece(new Rook(PieceColor.WHITE));

        //Knights
        tiles[0][1].setPiece(new Knight(PieceColor.BLACK));
        tiles[0][6].setPiece(new Knight(PieceColor.BLACK));
        tiles[7][1].setPiece(new Knight(PieceColor.WHITE));
        tiles[7][6].setPiece(new Knight(PieceColor.WHITE));

        //Bishops
        tiles[0][2].setPiece(new Bishop(PieceColor.BLACK));
        tiles[0][5].setPiece(new Bishop(PieceColor.BLACK));
        tiles[7][2].setPiece(new Bishop(PieceColor.WHITE));
        tiles[7][5].setPiece(new Bishop(PieceColor.WHITE));

        //Queen
        tiles[0][4].setPiece(new Queen(PieceColor.BLACK));
        tiles[7][4].setPiece(new Queen(PieceColor.WHITE));

        //King
        tiles[0][3].setPiece(new King(PieceColor.BLACK));
        tiles[7][3].setPiece(new King(PieceColor.WHITE));
    }


}
