package com.company.engine.board;

import com.company.chess.core.board.Tile;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TileTest {
    @Test
    void shouldConstructUnoccupied() {
        Tile tile = new Tile(0, 0);
        assertEquals(false, tile.isOccupied());
    }
}
