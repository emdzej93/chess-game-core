package com.company.engine.board;

import com.company.chess.core.board.Board;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class BoardTest {
    private static final int ROW_NUMBER = 8;
    private static final int COLUMN_NUMBER = 8;

    @BeforeEach
    void prepareBoard() {
        Board.getInstance().prepareBoard();
    }

    @Test
    void shouldBeSingleton() {
        Board boardOne = Board.getInstance();
        Board boardTwo = Board.getInstance();
        assertEquals(boardOne, boardTwo);
    }

    @Test
    void shouldCreateTiles() {
        Random generator = new Random();
        assertNotNull(Board
                .getInstance()
                .getTiles()
                [generator.nextInt(ROW_NUMBER)]
                [generator.nextInt(COLUMN_NUMBER)]);
    }

    @Test
    void noTileShouldBeSelected(){
        assertNull(Board.getInstance().getSelectedTile());
    }

    @Test
    void shouldSelectAndUnselectTile(){
        Board.getInstance().selectPiece(0, 0);
        assertNotNull(Board.getInstance().getSelectedTile());
        Board.getInstance().unselectAllPieces();
        assertNull(Board.getInstance().getSelectedTile());
    }

    @Test
    void shouldSelectPiece() {
        int row = 0;
        int column = 5;
        Board.getInstance().selectPiece(row, column);
        assertEquals(Board.getInstance().getSelectedTile().getRow(), row);
        assertEquals(Board.getInstance().getSelectedTile().getColumn(), column);
    }

    @Test
    void onlyOnePieceShouldBeSelected(){
        int rowOne = 0;
        int columnOne = 0;
        int rowTwo = 1;
        int columnTwo = 1;


        Board.getInstance().selectPiece(rowOne, columnOne);
        assertEquals(true, Board.getInstance().getTile(rowOne, columnOne).getPiece().isSelected());

        Board.getInstance().selectPiece(rowTwo, columnTwo);
        assertEquals(false, Board.getInstance().getTile(rowOne, columnOne).getPiece().isSelected());
        assertEquals(true, Board.getInstance().getTile(rowTwo, columnTwo).getPiece().isSelected());
    }
}
